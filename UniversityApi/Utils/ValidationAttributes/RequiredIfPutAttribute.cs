using System.ComponentModel.DataAnnotations;

using Microsoft.AspNetCore.Http;

namespace UniversityApi.Utils.ValidationAttributes
{
    public class RequiredIfPutAttribute : ValidationAttribute
    {
        public RequiredIfPutAttribute() : base("The {0} field is required") { }
        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            var contextMethod = (
                validationContext
                .GetService(typeof(IHttpContextAccessor)) 
                as IHttpContextAccessor
            )?.HttpContext?.Request.Method; 

            if (contextMethod == "PUT" && value is null)
            {
                string[] memberNames = validationContext.MemberName is null ? null : [ validationContext.MemberName ];
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName), memberNames);
            }

            return ValidationResult.Success;
        }
    }
}