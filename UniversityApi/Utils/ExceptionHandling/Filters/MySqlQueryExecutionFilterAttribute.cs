using System.Net;
using System.Net.Http;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

using MySql.Data.MySqlClient;

using MyEntityFramework.Exceptions;

namespace UniversityApi.Utils.ExceptionHandling.Filters
{
    public class MySqlQueryExecutionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception is QueryExecutionException qe && 
                qe.InnerException is MySqlException mysqle)
            {
                if (mysqle.Number == 1062)
                {
                    context.Result = new ObjectResult(
                        new HttpResponseMessage(HttpStatusCode.Conflict) {
                            Content = new StringContent(mysqle.Message)
                        }
                    );
                    context.ExceptionHandled = true;
                }
            }
        }
    }
}