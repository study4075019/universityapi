using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;

using MyEntityFramework.Exceptions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using UniversityApi.Services.Interfaces;

namespace UniversityApi.Utils.ExceptionHandling.Handlers
{
    public class DbConnectionFailedHandler : IExceptionHandler
    {
        private readonly ILogger<DbConnectionFailedHandler> _logger;
        private readonly IExceptionHandlerResponseHelper _ehService;
        public DbConnectionFailedHandler(
            ILogger<DbConnectionFailedHandler> logger,
            IExceptionHandlerResponseHelper ehService)
        {
            _logger = logger;
            _ehService = ehService;
        }
        public async ValueTask<bool> TryHandleAsync(HttpContext httpContext, Exception exception, CancellationToken cancellationToken)
        {
            if (exception is DbConnectionFailedException)
            {
                await _ehService.SetResponseAsync(
                    httpContext, 
                    exception, 
                    cancellationToken, 
                    StatusCodes.Status503ServiceUnavailable,
                    "about:blank",
                    "Error establishing connection to database"
                );  
                _logger.LogError(exception.Message);
                return true;
            }
            return false;
        }
    }
}