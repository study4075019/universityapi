using System;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.Extensions.Logging;

using UniversityApi.Services.Interfaces;

namespace UniversityApi.Utils.ExceptionHandling.Handlers
{
    public class GeneralExceptionHandler : IExceptionHandler
    {
        private readonly ILogger<GeneralExceptionHandler> _logger;
        private readonly IExceptionHandlerResponseHelper _ehService;

        public GeneralExceptionHandler(
            ILogger<GeneralExceptionHandler> logger,
            IExceptionHandlerResponseHelper ehService)
        {
            _logger = logger;
            _ehService = ehService;
        }

        public async ValueTask<bool> TryHandleAsync(HttpContext httpContext, Exception exception, CancellationToken cancellationToken)
        {
            await _ehService.SetResponseAsync(
                httpContext, 
                exception, 
                cancellationToken, 
                StatusCodes.Status500InternalServerError,
                "about:blank",
                "Something went wrong"
            );
            _logger.LogCritical(exception, exception.Message);
            return true;
        }
    }
}