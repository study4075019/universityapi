using System;

namespace UniversityApi.Utils.CustomConstraints
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ConstraintNameAttribute : Attribute
    {
        public ConstraintNameAttribute(string name) => Name = name;

        public readonly string Name;
    }
}