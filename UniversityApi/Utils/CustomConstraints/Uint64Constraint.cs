using System;
using System.Globalization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace UniversityApi.Utils.CustomConstraints
{
    [ConstraintName("ulong")]
    public class Uint64Constraint : IRouteConstraint
    {
        public bool Match(
            HttpContext? httpContext, 
            IRouter? route, 
            string routeKey, 
            RouteValueDictionary values, 
            RouteDirection routeDirection)
        {
            if (routeKey is null)
                throw new ArgumentNullException(nameof(routeKey));
            if (values is null)
                throw new ArgumentNullException(nameof(values));

            if (values.TryGetValue(routeKey, out object? value) && value != null)
            {
                if (value is ulong)
                {
                    return true;
                }

                var valueString = Convert.ToString(value, CultureInfo.InvariantCulture);
                return valueString is not null && CheckConstraintCore(valueString);
            }

            return false;
        }

        private static bool CheckConstraintCore(string valueString)
        {
            return ulong.TryParse(valueString, NumberStyles.Integer, CultureInfo.InvariantCulture, out _);
        }
    }
}