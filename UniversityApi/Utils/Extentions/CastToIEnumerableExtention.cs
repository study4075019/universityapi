using System.Collections.Generic;

namespace UniversityApi.Utils.Extentions
{
    public static class CastToIEnumerableExtention
    {
        public static IEnumerable<T> Yield<T>(this T item) { yield return item; }
    }
}