using System;
using System.Reflection;

using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;

using UniversityApi.Services.Interfaces;
using UniversityApi.Utils.CustomConstraints;

namespace UniversityApi.Utils.Extentions
{
    public static class ServiceCollectionExtentions
    {
        public static IServiceCollection AddCustomHttpConstraint<TConstraint>(this IServiceCollection services)
            where TConstraint : IRouteConstraint
        {
            var constraintName = typeof(TConstraint).GetCustomAttribute<ConstraintNameAttribute>()?.Name;
            if (string.IsNullOrEmpty(constraintName))
                throw new Exception($"Need custom attribute on constraint {typeof(TConstraint)}");
            services.Configure<RouteOptions>(cfg => cfg.ConstraintMap.Add(constraintName, typeof(TConstraint)));
            return services;
        }

        public static IServiceCollection AddExceptionHandlerResponseHelper<TImplementation>(this IServiceCollection services)
            where TImplementation : class, IExceptionHandlerResponseHelper
        {
            return services.AddSingleton<IExceptionHandlerResponseHelper, TImplementation>();
        }
    }
}