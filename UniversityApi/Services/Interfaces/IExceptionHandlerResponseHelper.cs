using System;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;

namespace UniversityApi.Services.Interfaces
{
    public interface IExceptionHandlerResponseHelper
    {
        public Task SetResponseAsync(
            HttpContext context, 
            Exception exception, 
            CancellationToken token, 
            int statusCode, 
            string type, 
            string detail
        );
    }
}