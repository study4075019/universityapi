using System;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;

using UniversityApi.Services.Interfaces;
using System.Linq;

namespace UniversityApi.Services.Implementations
{
    public class ExceptionHandlerResponseHelper : IExceptionHandlerResponseHelper
    {
        private readonly bool _isDevelopment;
        private readonly Regex _oneWordRegex;
        public ExceptionHandlerResponseHelper(IHostEnvironment henv)
        {
            _isDevelopment = henv.IsDevelopment();
            _oneWordRegex = new Regex("[A-Z][^A-Z]*");
        }

        public async Task SetResponseAsync(
            HttpContext context, 
            Exception exception, 
            CancellationToken token, 
            int statusCode, 
            string type, 
            string detail)
        {
            context.Response.StatusCode = statusCode;
            if (_isDevelopment)
            {
                context.Response.ContentType = "text/plain";
                await context.Response.WriteAsync($"{exception.GetType()}\n{exception.Message}:\n{exception.StackTrace}", token);
            }
            else
            {
                context.Response.ContentType = "application/json";
                var str = ((HttpStatusCode)context.Response.StatusCode).ToString();
                var matches = _oneWordRegex.Matches(str);
                var words = matches.Select(m => m.Value);
                var title = string.Join(" ", words);
                await context.Response.WriteAsJsonAsync(
                    new ProblemDetails
                    {
                        Status = context.Response.StatusCode,
                        Type = type,
                        Title = title,
                        Detail = detail,
                        Instance = context.Request.Host + context.Request.Path + context.Request.QueryString
                    },
                    token
                );
            }
        }
    }
}