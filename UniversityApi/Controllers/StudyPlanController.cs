using System.Net;
using System.Linq;
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

using UniversityApi.Models;
using UniversityApi.Repositories.Interfaces;

namespace UniversityApi.Controllers
{
    [ApiController, ApiVersion("1")]
    [Route("api/v{version:apiVersion}/study-plan")]
    public class StudyPlanController : ControllerBase
    {
        private readonly ILogger<StudyPlanController> _logger;
        private readonly IRepository<StudyPlan> _repository;

        public StudyPlanController(
            ILogger<StudyPlanController> logger, 
            IRepository<StudyPlan> repository) 
        {
            _logger = logger;
            _repository = repository;
        }

        [HttpPost]
        [ProducesResponseType(typeof(StudyPlan), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status400BadRequest)] // Validation - always
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status503ServiceUnavailable)] // Production
        [ProducesResponseType(typeof(string), StatusCodes.Status503ServiceUnavailable)] // Development
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)] // Production
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)] // Development
        public IActionResult Create([FromBody] StudyPlan studyPlan)
        {       
            StudyPlan? result = null;

            result = _repository.Create(studyPlan);

            return Created(
                Url.ActionLink(
                    protocol: RouteData.Values["protocol"]?.ToString(), 
                    host: RouteData.Values["host"]?.ToString(), 
                    controller: RouteData.Values["controller"]?.ToString(),
                    action: nameof(Read), 
                    values: new { id = result.Id }
                ), 
                result
            );
        }

        [HttpGet("{id:ulong:required}")]
        [ProducesResponseType(typeof(StudyPlan), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status503ServiceUnavailable)] // Production
        [ProducesResponseType(typeof(string), StatusCodes.Status503ServiceUnavailable)] // Development
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)] // Production
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)] // Development
        public IActionResult Read([FromRoute] ulong id)
        {
            StudyPlan? result = null;
            
            result = _repository.Read(id).FirstOrDefault();

            if (result is null)
            {
                return NoContent();
            }

            return Ok(result);
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<StudyPlan>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status503ServiceUnavailable)] // Production
        [ProducesResponseType(typeof(string), StatusCodes.Status503ServiceUnavailable)] // Development
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)] // Production
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)] // Development
        public IActionResult Read()
        {
            IEnumerable<StudyPlan>? result = null;

            result = _repository.Read();

            if (result is null || result.FirstOrDefault() is null)
            {
                return NoContent();
            }

            return Ok(result);
        }


        [HttpPut]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status400BadRequest)] // Validation - always
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status503ServiceUnavailable)] // Production
        [ProducesResponseType(typeof(string), StatusCodes.Status503ServiceUnavailable)] // Development
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)] // Production
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)] // Development
        public IActionResult Update([FromBody] StudyPlan studyPlan)
        {
            int updatedCount = 0;

            updatedCount = _repository.Update(studyPlan.Id, studyPlan);

            if (updatedCount == 0)
            {
                return NoContent();
            }

            return Ok();
        }

        [HttpDelete("{id:ulong:required}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status503ServiceUnavailable)] // Production
        [ProducesResponseType(typeof(string), StatusCodes.Status503ServiceUnavailable)] // Development
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)] // Production
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)] // Development
        public IActionResult Delete([FromRoute] ulong id)
        
        {
            int deletedCount = 0;

            deletedCount = _repository.Delete(id);

            if (deletedCount == 0)
            {
                return NoContent();
            }

            return Ok();
        }
    }
}