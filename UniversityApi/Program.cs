using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;

using MyEntityFramework.Extentions;

using UniversityApi.Models;
using UniversityApi.DataAccess.Contexts;
using UniversityApi.Repositories.Interfaces;
using UniversityApi.Repositories.Implementations;
using UniversityApi.Services.Implementations;
using UniversityApi.Utils.Extentions;
using UniversityApi.Utils.CustomConstraints;
using UniversityApi.Utils.ExceptionHandling.Handlers;

namespace UniversityApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddHttpContextAccessor();
            builder.Services.AddControllers();
            //.ConfigureApiBehaviorOptions(options => options.SuppressModelStateInvalidFilter = true);

            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            builder.Services.AddMyEFContext<UniversityMySqlContext>();

            builder.Services.AddMemoryCache();
            builder.Services.AddSingleton<IRepository<Faculty>, FacultyRepository>();
            builder.Services.AddSingleton<IRepository<Speciality>, SpecialityRepository>();
            builder.Services.AddSingleton<IRepository<StudyPlan>, StudyPlanRepository>();
            builder.Services.AddSingleton<IRepository<Subject>, SubjectRepository>();


            builder.Services.AddCustomHttpConstraint<Uint64Constraint>();

            builder.Services.AddApiVersioning();

            builder.Services.AddExceptionHandler<DbConnectionFailedHandler>();
            builder.Services.AddExceptionHandler<GeneralExceptionHandler>();
            builder.Services.AddExceptionHandlerResponseHelper<ExceptionHandlerResponseHelper>();

            var app = builder.Build();

            app.UseExceptionHandler(_ => { });
            app.UseStatusCodePages();
            //app.UseDeveloperExceptionPage();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}
