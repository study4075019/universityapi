using System;
using System.Linq;
using System.Collections.Generic;

using Microsoft.Extensions.Caching.Memory;

using MyEntityFramework.Core;

using UniversityApi.Utils.Extentions;
using UniversityApi.DataAccess.DbModels;
using UniversityApi.Repositories.Interfaces;

namespace UniversityApi.Repositories.CacheRepository
{
    public abstract class CacheRepositoryBase<TEntity> : IRepository<TEntity>
        where TEntity : class, new()
    {
        protected readonly IDbContext<UniversityDbModel> _context;
        protected readonly IMemoryCache _cache;

        protected CacheRepositoryBase(
            IDbContext<UniversityDbModel> context, 
            IMemoryCache cache,
            TimeSpan? expitationTime = null)
        {
            _context = context;
            _cache = cache;
            if (expitationTime is null)
                ExpirationTime = ExpirationTimeDefault;
            else
                ExpirationTime = (TimeSpan)expitationTime;
        }

        public virtual TimeSpan ExpirationTimeDefault => new TimeSpan(0, 5, 0);

        public virtual TimeSpan ExpirationTime { get; set; }

        public virtual TEntity Create(TEntity entity)
        {
            var result = _context.DbModel.GetDbEntity<TEntity>().CreateGet(entity);
            _cache.Set(new CacheKey<TEntity>(result), result, new TimeSpan(0, 5, 0));
            return result;
        }

        public virtual IEnumerable<TEntity> Read(object? key = null)
        {
            if (key is not null)
            {
                if (_cache.TryGetValue(new CacheKey<TEntity>(key), out TEntity? entity))
                {
                    return entity.Yield();
                }
                else
                {
                    var readResult = _context.DbModel.GetDbEntity<TEntity>().Read(key);
                    var first = readResult.FirstOrDefault();
                    if (first is not null)
                    {
                        _cache.Set(new CacheKey<TEntity>(key), first, new TimeSpan(0, 5, 0));
                    }
                    return readResult;
                }
            }
            
            return _context.DbModel.GetDbEntity<TEntity>().Read();
        }

        public virtual int Update(object keyValue, TEntity entity)
        {
            var updated = _context.DbModel.GetDbEntity<TEntity>().Update(keyValue, entity);
            if (updated > 0)
            {
                _cache.Set(new CacheKey<TEntity>(keyValue), entity, new TimeSpan(0, 5, 0));
            }
            return updated;
        }

        public virtual int Delete(object keyValue)
        {
            var deleted = _context.DbModel.Faculties.Delete(keyValue);
            _cache.Remove(new CacheKey<TEntity>(keyValue)); 
            return deleted;         
        }
    }
}