using System;
using System.Reflection;

namespace UniversityApi.Repositories.CacheRepository
{
    public class CacheKey<TEntity> : IEquatable<CacheKey<TEntity>>
    {
        public object CacheKeyValue { get; set; }

        public CacheKey(object key) => CacheKeyValue = key;

        public override bool Equals(object? obj)
        {
            if (obj is CacheKey<TEntity> ck)
            {
                var keyType = CacheKeyValue.GetType();
                if (keyType.IsAssignableTo(typeof(IEquatable<>).MakeGenericType(keyType)))
                {
#pragma warning disable
                    return (bool)keyType.GetMethod(
                        "Equals", 
                        BindingFlags.Instance | 
                        BindingFlags.Public, 
                        [ keyType ]
                    ).Invoke(CacheKeyValue, [ ck.CacheKeyValue ]);
#pragma warning restore
                }
                return CacheKeyValue.Equals(ck.CacheKeyValue);
            }
            return false;
        }

        public bool Equals(CacheKey<TEntity>? ck)
        {
            if (ck is null)
            {
                return false;
            }

            return CacheKeyValue.Equals(ck.CacheKeyValue);
        }

        public override int GetHashCode()
        {
            return CacheKeyValue.GetHashCode() + typeof(TEntity).GetHashCode();
        }
    }
}