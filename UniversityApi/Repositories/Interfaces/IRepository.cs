using System.Collections.Generic;

namespace UniversityApi.Repositories.Interfaces 
{
    public interface IRepository<TModel>
    {
        public TModel Create(TModel entity);
        public IEnumerable<TModel> Read(object? key = null);
        public int Update(object keyValue, TModel entity);
        public int Delete(object key);
    }
}