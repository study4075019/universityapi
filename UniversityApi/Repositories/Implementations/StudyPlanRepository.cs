using System;

using Microsoft.Extensions.Caching.Memory;

using MyEntityFramework.Core;

using UniversityApi.Models;
using UniversityApi.DataAccess.DbModels;
using UniversityApi.Repositories.CacheRepository;

namespace UniversityApi.Repositories.Implementations
{
    public class StudyPlanRepository : CacheRepositoryBase<StudyPlan>
    {
        public StudyPlanRepository(
            IDbContext<UniversityDbModel> context, 
            IMemoryCache cache
        ) : base(context, cache) { } 

        public override TimeSpan ExpirationTimeDefault => new TimeSpan(0, 10, 0);

        public override StudyPlan Create(StudyPlan studyPlan)
        {
            studyPlan.Id = null;
            return base.Create(studyPlan);
        }
    }
}