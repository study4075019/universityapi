using Microsoft.Extensions.Caching.Memory;

using MyEntityFramework.Core;

using UniversityApi.Models;
using UniversityApi.DataAccess.DbModels;
using UniversityApi.Repositories.CacheRepository;

namespace UniversityApi.Repositories.Implementations
{
    public class FacultyRepository : CacheRepositoryBase<Faculty>
    {
        public FacultyRepository(
            IDbContext<UniversityDbModel> context, 
            IMemoryCache cache
        ) : base(context, cache) { } 

        public override Faculty Create(Faculty faculty)
        {
            faculty.Id = null;
            return base.Create(faculty);
        }
    }
}