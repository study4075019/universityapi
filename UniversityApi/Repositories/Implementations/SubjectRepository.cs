using Microsoft.Extensions.Caching.Memory;

using MyEntityFramework.Core;

using UniversityApi.Models;
using UniversityApi.DataAccess.DbModels;
using UniversityApi.Repositories.CacheRepository;

namespace UniversityApi.Repositories.Implementations
{
    public class SubjectRepository : CacheRepositoryBase<Subject>
    {
        public SubjectRepository(
            IDbContext<UniversityDbModel> context, 
            IMemoryCache cache
        ) : base(context, cache) { } 

        public override Subject Create(Subject subject)
        {
            subject.Id = null;
            return base.Create(subject);
        }
    }
}