using System;

using Microsoft.Extensions.Caching.Memory;

using MyEntityFramework.Core;

using UniversityApi.Models;
using UniversityApi.DataAccess.DbModels;
using UniversityApi.Repositories.CacheRepository;

namespace UniversityApi.Repositories.Implementations
{
    public class SpecialityRepository : CacheRepositoryBase<Speciality>
    {
        public SpecialityRepository(
            IDbContext<UniversityDbModel> context, 
            IMemoryCache cache
        ) : base(context, cache) { }

        public override TimeSpan ExpirationTimeDefault => new TimeSpan(0, 10, 0);

        public override Speciality Create(Speciality speciality)
        {
            speciality.Id = null;
            return base.Create(speciality);
        }
    }
}