using MyEntityFramework.Core;

using UniversityApi.DataAccess.DbModels;
using UniversityApi.DataAccess.DbClientProviders;

namespace UniversityApi.DataAccess.Contexts
{
    public class UniversityMySqlContext : DbContextBase<UniversityDbModel, MySqlClientProvider>
    {
        public UniversityMySqlContext(DbContextDependencies<MySqlClientProvider> dependencies) 
            : base (dependencies) { }

        protected override string ConnectionStringName => "DefaultConnection";
    }
}