using MySql.Data.MySqlClient;

using MyEntityFramework.Core;

namespace UniversityApi.DataAccess.DbClientProviders
{
    public class MySqlClientProvider 
        : DbClientProviderBase<
            MySqlCommand, 
            MySqlConnection, 
            MySqlParameter>
    { }
}