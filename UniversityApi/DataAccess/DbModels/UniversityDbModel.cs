using MyEntityFramework.Core;

using UniversityApi.Models;

namespace UniversityApi.DataAccess.DbModels
{
    public class UniversityDbModel : DbModelBase
    {
#pragma warning disable
        public DbEntity<Faculty> Faculties { get; private set; }
        public DbEntity<Speciality> Specs { get; private set; }
        public DbEntity<Subject> Subjects { get; private set; }
        public DbEntity<StudyPlan> StudyPlan { get; private set; }
#pragma warning restore
    }
}