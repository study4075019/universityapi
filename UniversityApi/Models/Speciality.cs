using System.ComponentModel.DataAnnotations;

using MyEntityFramework.Attributes;

using UniversityApi.Utils.ValidationAttributes;

namespace UniversityApi.Models
{
    [Table("Speciality")]
    public class Speciality : ModelBase
    {
        [PrimaryKey, Column("Id"), RequiredIfPut]
        public uint? Id { get; set; }

        [Column("Name"), Required, MaxLength(50)]
        public string Name { get; set; } = string.Empty;
        
        [Column("FacultyId"), Required]
        public uint FacultyId { get; set; }
    }
}

/*
    CREATE TABLE Speciality (
        Id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT, 
        Name VARCHAR(50) NOT NULL, 
        FacultyId BIGINT UNSIGNED NOT NULL, 
        PRIMARY KEY (Id), 
        FOREIGN KEY (FacultyId) REFERENCES Faculty(Id)
    );
*/