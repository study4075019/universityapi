using System.Linq;

namespace UniversityApi.Models
{
    public abstract class ModelBase
    {
        public override int GetHashCode()
        {
            return GetType().GetProperties().Select(p => p.GetValue(this).GetHashCode()).Sum();
        }
    }
}