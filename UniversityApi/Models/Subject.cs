using System.ComponentModel.DataAnnotations;

using MyEntityFramework.Attributes;

using UniversityApi.Utils.ValidationAttributes;

namespace UniversityApi.Models
{
    [Table("Subject")]
    public class Subject : ModelBase
    {
        [PrimaryKey, Column("Id"), RequiredIfPut]
        public uint? Id { get; set; }

        [Column("Name"), Required, MaxLength(50)]
        public string Name { get; set; } = string.Empty;
    }
}

/*
    CREATE TABLE Faculty (
        Id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT, 
        Name VARCHAR(50) NOT NULL, 
        PRIMARY KEY (Id)
    );
*/