using System.ComponentModel.DataAnnotations;

using MyEntityFramework.Attributes;

using UniversityApi.Utils.ValidationAttributes;

namespace UniversityApi.Models
{
    [Table("StudyPlan")]
    public class StudyPlan : ModelBase
    {
        [PrimaryKey, Column("Id"), RequiredIfPut]
        public uint? Id { get; set; }

        [Column("SpecialityId"), Required]
        public uint SpecialityId { get; set; }

        [Column("SubjectId"), Required]
        public uint SubjectId { get; set; }
    }
}

/*
    CREATE TABLE StudyPlan (
        Id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT, 
        SpecialityId BIGINT UNSIGNED NOT NULL, 
        SubjectId BIGINT UNSIGNED NOT NULL,
        PRIMARY KEY (Id),
        FOREIGN KEY (SpecialityId) REFERENCES Speciality(Id),
        FOREIGN KEY (SubjectId) REFERENCES Subject(Id)
    );
*/