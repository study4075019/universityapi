using System.ComponentModel.DataAnnotations;

using MyEntityFramework.Attributes;

using UniversityApi.Utils.ValidationAttributes;

namespace UniversityApi.Models
{
    [Table("Faculty")]
    public class Faculty : ModelBase
    {
        //[ValidationIf([ typeof(PostMethodConditionProvider) ], typeof(NullNilAttribute), [])]
        //[ValidationIf([ typeof(PutMethodConditionProvider) ], typeof(RequiredAttribute), [])]
        [PrimaryKey, Column("Id"), RequiredIfPut]
        public ulong? Id { get; set; }

        [Column("Name"), Required, MaxLength(50)]
        public string Name { get; set; } = string.Empty;
    }
}

/*
    CREATE TABLE Faculty (
        Id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT, 
        Name VARCHAR(50) NOT NULL, 
        PRIMARY KEY (Id)
    );
*/